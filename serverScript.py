import serial
import socket
import sys
from thread import *
import robotController

#Robot USB robotPort
robotPort = "" 

#Socket parameters
HOST = ''
PORT = 8880  #Default port

#Flag variables
activeConnection = False;

#Super mega secret auth code
authCode = "USIC"

#Try to connect to robot port
for x in xrange(0,3):
	try:
		robotPort = "/dev/ttyACM"+str(x)
		if robotController.connect(robotPort) == True:
			print 'Connected to robot. Port:' + robotPort
			break
	except Exception, e:
		pass

if robotController.robot == None:
	print "Could not connect to robot"
	sys.exit()
		
#Get launch network parameters and set parameters
if len(sys.argv)>1:
    PORT = int(sys.argv[1])
else:
    print "Used default network port"
print "Network port: " + str(PORT) + " Robot USB port: " + str(robotPort)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'

#Bind socket to local host and robotPort
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
print 'Socket bind complete'
#Start listening on socket
s.listen(1)
print 'Socket now listening'

#Function for handling connections. This will be used to create threads
def clientthread(conn):
    #infinite loop so that function do not terminate and thread do not end.
    while True:
        #Receiving from client
        data = conn.recv(1024).rstrip()
        
        if data == "SCAN" and robotController.scanning == False:
            robotController.scanning = True;
            start_new_thread(robotController.scan, (conn,))
            pass
        else:
            try:
                robotController.driver(data)
            except Exception as ex:
                conn.sendall(ex.args[0])
                break
            pass
        
        if not data:
        	break
        
    #came out of loop
    global activeConnection
    activeConnection = False
    robotController.scanning = False
    print 'connection closed'
    conn.close()

#Waiting for connection
while True:
    	conn, addr = s.accept()
    	tmpAdress = int(addr[1])
    	
        recCode = conn.recv(1024).rstrip();

        if activeConnection == False and recCode == authCode:
            print 'Connected with ' + addr[0] + ':' + str(addr[1])
            start_new_thread(clientthread ,(conn,))
            activeConnection = True
            pass
        else:
            conn.sendall("Bad connection")
            conn.close()
            pass
    	pass


s.close()
