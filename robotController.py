import serial
import time
import struct
import math


#Robot commands
movement = {
'FRONT' : "1",
'BACK' : "2",
'LEFT' : "3",
'RIGHT' : "4",
'STOP' : "0"
}

ir = {
'LEFT': "5",
'RIGHT': "6"
}

servo = {
"LEFT": "7",
"RIGHT": "8",
"START": "9"
}

robot = None

scanning = False;


def driver(var):
	if var == "STOP":
		global scanning
		scanning = False
		pass

	if var in movement:
		sendCommand(movement[var])
		print movement[var]

def connect(port):
	print "Connecting to robot at port - " + port
	global robot
	try:
		robot = serial.Serial(port, 9600)
	except serial.serialutil.SerialException:
		print 'Could not open port' + port
		return False
	time.sleep(3)
	print "Connected to robot"
	return True 

def sendCommand(command):
	if robot != None:
		robot.write(command)
	else:
		raise Exception("No connection to robot")

def move(direction):
	sendCommand(movement[direction])

def moveForward():
    sendCommand(movement["FRONT"])

def moveBackwards():
    sendCommand(movement["BACK"])

def turnLeft():
    sendCommand(movement["LEFT"])

def turnRight():
    sendCommand(movement["RIGHT"])
    
def stop(): 
    sendCommand(movement["STOP"])
    
def getIR(position):
        robot.flushInput()
        sendCommand(ir[position])
        return struct.unpack('B', robot.read())[0]

def servoLeft(times):
	for x in xrange(times):
		sendCommand(servo["LEFT"])

def servoRight(times):
	for x in xrange(times):
		sendCommand(servo["RIGHT"])

def servoStart(delay):
	sendCommand(servo["START"])
	if delay == True:
		time.sleep(1)

def sendData(conn, x, y):
	conn.sendall("["+str(x)+"/"+str(y)+"]")

def scan(conn):
	global scanning
	print 'Scan'
	scanPos(conn, 5, 5)

	conn.sendall("ENDL")
		


def scanPos(conn, rx, ry):
	print "LEFT"
	servoStart(True)
	for i in xrange(90):
		ir = getIR("LEFT")
		print ir
		if ir > 20 and ir < 60:
			x = (ir * math.sin(math.radians(-i))) + rx
			y = (ir * math.cos(math.radians(-i))) + ry
			sendData(conn, int(x), int(y))
		print "T"
		servoLeft(1)
		time.sleep(0.05)

	print "RIGHT"
	servoStart(True)
	for i in xrange(90):
                ir = getIR("LEFT")
		print ir
                if ir > 20 and ir < 60:
                        x = (ir * math.sin(math.radians(i))) + rx
                        y = (ir * math.cos(math.radians(i))) + ry
                        sendData(conn, int(x), int(y))
                servoRight(1)
		time.sleep(0.05)
	
	servoStart(False)


def move(sec):
	moveForward()
	time.sleep(sec)
	stop()				
